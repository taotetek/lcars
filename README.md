# lcars
Notes for my Raspberry Pi / Kubernetes Experiments

## Useful Links

* [Serverless Kubernetes home-lab with your Raspberry Pis](https://blog.alexellis.io/serverless-kubernetes-on-raspberry-pi/)
* [Raspian Stretch](https://www.raspberrypi.org/downloads/raspbian/)
* [Grafana docker image for raspberry pi](https://github.com/phueper/rpi-grafana-docker)
* [Grafana packages for raspberry pi](https://github.com/fg2it/grafana-on-raspberry)
* [BATMAN Mesh Networking](https://www.open-mesh.org/projects/batman-adv/wiki)


## Parts / Hardware
* [Geuxrobot Raspberry Pi Model B Dog Bone Case Stackable Kit](https://www.amazon.com/Geauxrobot-Raspberry-Model-Stackable-Works/dp/B00MRLM9QS)
* [RAVPower 60W 12A 6-Port USB Charger](https://www.amazon.com/RAVPower-Charger-Desktop-Charging-Technology/dp/B00OQ19QYA/ref=sr_1_5?s=electronics&ie=UTF8&qid=1516193388&sr=1-5&keywords=usb+power+hub)
* [Sabrent 6-Packl 1ft Micro USB Cables](https://www.amazon.com/Sabrent-6-Pack-Premium-Cables-CB-UM61/dp/B011KMSNXM)
* [GL.iNet GL-AR300M Mini Travel Router](https://www.amazon.com/GL-iNet-GL-AR300M-Pre-installed-Performance-Programmable/dp/B01K6MHRJI/)
* [Black Box 8 Port USB Powered Switch](https://www.amazon.com/BLACK-LBS008A-USB-Powered-8-Port-Switch/dp/B0148J50EY)
* [Blinkt! 1x8 LED lights](https://shop.pimoroni.com/products/blinkt)
