try:
    from zyre_pyzmq import Zyre as Pyre
except Exception as e:
    print("using Python native module", e)
    from pyre import Pyre

from pyre import zhelper
import zmq
import json
import sys
import uuid
import socket
import time
import blinkt

NODES = {}
NODES["lcars1"] = 0
NODES["lcars2"] = 1 
NODES["lcars3"] = 2
NODES["lcars4"] = 3
NODES["lcars5"] = 4
NODES["lcars6"] = 5

def ping_task(ctx, pipe):
    n = Pyre("PING")
    n.set_header("HOSTNAME_HEADER", socket.gethostname())
    n.join("PING")
    n.start()

    poller = zmq.Poller()
    poller.register(pipe, zmq.POLLIN)
    poller.register(n.socket(), zmq.POLLIN)

    nodeinfo = {}

    while(True):
        items = dict(poller.poll())
        if pipe in items and items[pipe] == zmq.POLLIN:
            message = pipe.recv()
            if message.decode('utf-8') == "$$STOP":
                break
            n.shouts("PING", message.decode('utf-8'))
        
        elif n.socket() in items and items[n.socket()] == zmq.POLLIN:
            cmds = n.recv()
            msg_type = cmds.pop(0)
            node_id = uuid.UUID(bytes=cmds.pop(0))
            msg_name = cmds.pop(0)
            print(node_id)
            print(msg_name)
            if msg_type.decode('utf-8') == "SHOUT":
                print("NODE_MSG_GROUP: %s" % cmds.pop(0))
                blinkt.set_pixel(NODES[nodeinfo[node_id]], 0, 128, 0)
                blinkt.show()
                blinkt.set_pixel(NODES[nodeinfo[node_id]], 0, 0, 0)
                blinkt.show()
            
            elif msg_type.decode('utf-8') == "ENTER":
                headers = json.loads(cmds.pop(0).decode('utf-8'))
                hostname = headers["HOSTNAME_HEADER"]
                nodeinfo[node_id] = hostname

            print(nodeinfo)
    n.stop()


if __name__ == '__main__':
    ctx = zmq.Context()
    ping_pipe = zhelper.zthread_fork(ctx, ping_task)

    msg = "HELLO"

    while True:
        try:
            ping_pipe.send(msg.encode('utf_8'))
        except (KeyboardInterrupt, SystemExit):
            break
        time.sleep(1)

    ping_pipe.send("$$STOP".encode('utf_8'))
    print("FINISHED")
